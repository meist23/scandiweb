<?php
require_once "product.php";

class DVD extends Product
{
    private $dvd_size;

    function get_dvd_size()
    {
        return $this->dvd_size;
    }
    function __construct()
    {
        $this->dvd_size = 0;
    }
    function setProduct($idproduct, $sku, $name, $price, $productType, $data = [])
    {
        $this->idproduct = $idproduct;
        $this->sku = $sku;
        $this->name = $name;
        $this->price = $price;
        $this->productType = $productType;
        $this->dvd_size = $data['size'];
    }
    function addProduct($sku, $name, $price, $productType, $data = [])
    {
        $DB = new Database();
        $arr['sku'] = $sku;
        $arr['name'] = $name;
        $arr['price'] = $price;
        $arr['productType'] = $productType;
        $arr['dvd_size'] = $data['size'];
        show($arr);
        $DB->write($arr);
    }
    function showProductData()
    {
        echo "<p>Size: $this->dvd_size MB</p>";
    }
}
