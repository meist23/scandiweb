<?php

class Database
{
    private $DB;
    function __construct()
    {
        $this->DB = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);
    }
    public function read($query)
    {
        $check = $stm = $this->DB->query($query);
        if ($check) {
            return $stm->fetch_all(MYSQLI_ASSOC);
        } else {
            return false;
        }
    }
    public function write($data = [])
    {
        $query = "insert into products (sku,name,price,product_type,size,weight,height,width,length) values 
            (?,?,?,?,?,?,?,?,?)";
        $stm = $this->DB->prepare($query);
        
        $stm->bind_param("ssisiiiii", $data['sku'], $data['name'], $data['price'], $data['productType'], $data['dvd_size'], $data['book_weight'], $data['furniture_height'], $data['furniture_width'], $data['furniture_length']);
        if (count($data) == 0) {
            $stm = $this->DB->query($query);
            $check = 0;
            if ($stm) {
                $check = 1;
            }
        } else {
            $check = $stm->execute();
        }
        if ($check) {
            return true;
        } else {
            return false;
        }
    }
    public function delete($data = [])
    {
        $query = "delete from products where idproduct = ?";
        $stm = $this->DB->prepare($query);

        $stm->bind_param("i", $data['id']);
        if (count($data) == 0) {
            $stm = $this->DB->query($query);
            $check = 0;
            if ($stm) {
                $check = 1;
            }
        } else {
            $check = $stm->execute();
        }
        if ($check) {
            return true;
        } else {
            return false;
        }
    }
}
