<?php
require_once "product.php";

class Furniture extends Product
{
    private $furniture_height;
    private $furniture_width;
    private $furniture_length;
    function __construct()
    {
        $this->furniture_height = 0;
        $this->furniture_width = 0;
        $this->furniture_length = 0;
    }
    function setProduct($idproduct, $sku, $name, $price, $productType, $data = [])
    {
        $this->idproduct = $idproduct;
        $this->sku = $sku;
        $this->name = $name;
        $this->price = $price;
        $this->productType = $productType;
        $this->furniture_height = $data['height'];
        $this->furniture_width = $data['width'];
        $this->furniture_length = $data['length'];
    }
    function get_furniture_height()
    {
        return $this->furniture_height;
    }
    function get_furniture_width()
    {
        return $this->furniture_width;
    }
    function get_furniture_length()
    {
        return $this->furniture_length;
    }
    function addProduct($sku, $name, $price, $productType, $data = [])
    {
        $DB = new Database();

        $arr['sku'] = $sku;
        $arr['name'] = $name;
        $arr['price'] = $price;
        $arr['productType'] = $productType;
        $arr['furniture_height'] = $data['height'];
        $arr['furniture_width'] = $data['width'];
        $arr['furniture_length'] = $data['length'];

        $DB->write($arr);
    }
    function showProductData()
    {
        echo "<p>Dimension: $this->furniture_height", 'x', "$this->furniture_width", 'x', "$this->furniture_length </p>";
    }
}
