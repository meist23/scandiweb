<?php

class Controller
{
    protected function view($view, $data = [])
    {
        if (file_exists("app/views/" . $view . ".php")) {
            include "app/views/" . $view . ".php";
        } else {
            //if file failed to load or not exist/found
            include "app/views/404.php";
        }
    }
    protected function loadFunction($functions)
    {
        if (file_exists("app/functions/" .  strtolower($functions) . ".php")) {
            include_once "app/functions/" . strtolower($functions) . ".php";
            return $functions = new $functions();
        }
        return false;
    }
}
