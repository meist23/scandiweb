<?php

class Product
{
    protected $idproduct;
    protected $sku;
    protected $name;
    protected $price;
    protected $productType;

    function __construct()
    {
        $this->sku = "";
        $this->name = "";
        $this->price = "";
        $this->productType = "";
    }
    function setProduct($idproduct, $sku, $name, $price, $productType, $data = [])
    {
    }

    function get_idProduct(){
        return $this->idproduct;
    }
    function get_sku()
    {
        return $this->sku;
    }
    function get_name()
    {
        return $this->name;
    }
    function get_price()
    {
        return $this->price;
    }
    function get_productType()
    {
        return $this->productType;
    }
    function showProductData()
    {
    }

    function get_all()
    {
        $DB = new Database();
        $query = "select * from products";

        $data = $DB->read($query);
        if (is_array($data)) {
            return $data;
        }
        return false;
    }
    function addProduct($sku, $name, $price, $productType, $data = [])
    {
    }
    function deleteProduct($id)
    {
        $DB = new Database();
        $arr['id'] = $id;
        $data = $DB->delete($arr);
        if (is_array($data)) {
            return $data;
        }
    }
}
