<?php
class App
{
    private $controller = "mainproduct";
    private $method = "index";
    private $params = [];

    public function __construct()
    {
        //GET CONTROLLER/VIEW
        $url = $this->splitURL();
        $checkDash = str_replace("-", "", strtolower($url[0]));
        if (file_exists("app/controllers/" . str_replace("-", "", strtolower($url[0])) . ".php")) {
            $this->controller = strtolower($checkDash);
            unset($url[0]);
        }
        require "app/controllers/" . $this->controller . ".php";
        $this->controller = new $this->controller;
        //GET METHOD
        if (isset($url[1])) {
            if (method_exists($this->controller, $url[1])) {
                $this->method = $url[1];
                unset($url[1]);
            }
        }
        //GET PARAM INPUT
        $this->params = array_values($url);
        call_user_func_array([$this->controller, $this->method], $this->params);
    }

    private function splitURL()
    {
        $url = isset($_GET['url']) ? $_GET['url'] : "mainproduct";
        return explode("/", filter_var(trim($url, "/"), FILTER_SANITIZE_URL));
    }
}
