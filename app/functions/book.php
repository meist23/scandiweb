<?php
require_once "product.php";

class Book extends Product
{
    private $book_weight;

    function __construct()
    {
        $this->book_weight = 0;
    }
    function setProduct($idproduct, $sku, $name, $price, $productType, $data = [])
    {
        $this->idproduct = $idproduct;
        $this->sku = $sku;
        $this->name = $name;
        $this->price = $price;
        $this->productType = $productType;
        $this->book_weight = $data['weight'];
    }
    function get_book_weight()
    {
        return $this->book_weight;
    }
    function addProduct($sku, $name, $price, $productType, $data = [])
    {
        $DB = new Database();
        $arr['sku'] = $sku;
        $arr['name'] = $name;
        $arr['price'] = $price;
        $arr['productType'] = $productType;
        $arr['book_weight'] = $data['weight'];

        $DB->write($arr);
    }
    function showProductData()
    {
        echo "<p>Weight: $this->book_weight KG</p>";
    }
}
