<?php

class MainProduct extends Controller
{
    function index()
    {
        $products = $this->loadFunction("product");
        if (isset($_POST['delete-product-btn'])) {
            if (isset($_POST['check_selected'])) {
                foreach ($_POST['check_selected'] as $search) {
                    $products->deleteProduct($search);
                }
            }
        }
        $result = $products->get_all();
        if (!empty($result)) {
            $data = array(count($result));
        } else {
            $data = 0;
        }
        for ($i = 0; $i < count($result); $i++) {
            $this->loadFunction($result[$i]['product_type']);
            $data[$i] = new $result[$i]['product_type'];
            $idproduct = $result[$i]['idproduct'];
            $sku = $result[$i]['sku'];
            $name = $result[$i]['name'];
            $price = $result[$i]['price'];
            $product_type = $result[$i]['product_type'];
            $product['size'] = $result[$i]['size'];
            $product['weight'] = $result[$i]['weight'];
            $product['height'] = $result[$i]['height'];
            $product['width'] = $result[$i]['width'];
            $product['length'] = $result[$i]['length'];
            $data[$i]->setProduct($idproduct, $sku, $name, $price, $product_type, $product);
        }
        $this->view("mainProduct", $data);
    }
}
