<?php

class AddProduct extends Controller
{
    function index()
    {
        header("location:" . ROOT . "add-product/add");
        die;
    }
    function add()
    {
        if (isset($_POST['sku']) && isset($_POST['name']) && isset($_POST['price']) && isset($_POST['productType'])) {
            if (isset($_POST['dvd_size']) || isset($_POST['book_weight']) || isset($_POST['furniture_height']) && isset($_POST['furniture_width']) && isset($_POST['furniture_length'])) {
                $arr['size'] = $_POST['dvd_size'];
                $arr['weight'] = $_POST['book_weight'];
                $arr['height'] = $_POST['furniture_height'];
                $arr['width'] = $_POST['furniture_width'];
                $arr['length'] = $_POST['furniture_length'];
                $addData = $this->loadFunction($_POST['productType']);
                $addData->addProduct($_POST['sku'], $_POST['name'], $_POST['price'], $_POST['productType'], $arr);
                header("location:" . ROOT . "");
                die;
            }
        }
        $this->view("addProduct");
    }
}
