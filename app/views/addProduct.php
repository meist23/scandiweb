<!DOCTYPE html>
<html lang="en">

<head>
  <link rel="stylesheet" type="text/css" href="<?= ASSETS ?>/css.css">
  <title>Product Add</title>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
</head>

<body>
  <header class="header">
    <div class="container header-container">
      <h1>
        Product Add
      </h1>
      <ul class="nav main-nav">
        <li><button id="submitBtn" type="submit" form="product_form" class="right">Save</button></li>
        <li><button id="cancelbtn" type="button" class="right">Cancel</button></li>
      </ul>
    </div>
  </header>
  <section class="section">
    <form class="container section-container" id="product_form" method="post">
      <div class="input-field">
        <label>SKU:</label>
        <input type="text" name="sku" id="sku" class="input" placeholder="Enter SKU">
      </div>
      <div class="input-field">
        <label>Name:</label>
        <input type="text" name="name" id="name" class="input" placeholder="Enter Name">
      </div>
      <div class="input-field">
        <label>Price ($)</label>
        <input type="number" name="price" id="price" class="input" placeholder="Enter Price">
      </div>
      <div class="input-field">
        <label>Type Switcher</label>
        <select id="productType" name="productType" class="input">
          <option value="" selected disabled hidden>Select Product Type</option>
          <option value="DVD">DVD</option>
          <option value="Book">Book</option>
          <option value="Furniture">Furniture</option>
        </select>
      </div>
      <div id="productSelector">
        <div class="selectedProduct" id="DVD">
          <div class="input-field">
            <label>Size (MB)</label>
            <input type="number" name="dvd_size" id="size" class="form-control" placeholder="Enter DVD Size" class="input">
          </div>
          <p>Please input in MB</p>
        </div>
        <div class="selectedProduct" id="Book">
          <div class="input-field">
            <label>Weight (KG)</label>
            <input type="number" name="book_weight" id="weight" class="form-control" placeholder="Enter Book Weight" class="input">
          </div>
          <p>Please input in KG</p>
        </div>
        <div class="selectedProduct" id="Furniture">
          <div class="input-field">
            <label>Height (CM)</label>
            <input type="number" name="furniture_height" id="height" class="form-control" placeholder="Enter Furniture Height" class="input">
          </div>
          <div class="input-field">
            <label>Width (CM)</label>
            <input type="number" name="furniture_width" id="width" class="form-control" placeholder="Enter Furniture Width" class="input">
          </div>
          <div class="input-field">
            <label>Length (CM)</label>
            <input type="number" name="furniture_length" id="length" class="form-control" placeholder="Enter Furniture Length" class="input">
          </div>
          <p>Please input in Height x Width x Length format</p>
        </div>
      </div>
    </form>
  </section>

  <?php $this->view("footer", $data); ?>

  <script>
    const fields = document.getElementById("productSelector").querySelectorAll(".selectedProduct")
    const submitButton = document.getElementById('submitBtn');
    const form = document.getElementById("product_form");
    form.addEventListener('submit', function(e) {
      const isEmpty = str => !str.trim().length
      const sku = document.getElementById("sku").value
      const name = document.getElementById("name").value
      const price = document.getElementById("price").value
      const selectedProduct = document.getElementById("productType").value
      if (isEmpty(sku) || isEmpty(name) || isEmpty(price) || isEmpty(selectedProduct)) {
        alert('Please, submit required data');
        e.preventDefault();
      } else if (isNaN(price)) {
        alert('Please, provide the data of indicated type');
        e.preventDefault();
      } else {
        let checkEmptySelectedProduct = false;
        let checkStringSelectedProduct = false;
        for (const input of fields) {
          if (input.style.display != 'none') {
            inputs = input.querySelectorAll('input');
            for (const productInput of inputs) {
              if (isEmpty(productInput.value)) {
                checkEmptySelectedProduct = true;
                break;
              } else if (isNaN(productInput.value)) {
                checkStringSelectedProduct = true;
                break;
              }
            }
          }
        }
        if (checkEmptySelectedProduct) {
          alert('Please, submit required data!');
          e.preventDefault();
        } else if (checkStringSelectedProduct) {
          alert('Please, provide the data of indicated type');
          e.preventDefault();
        } else {}
      }
    });
    const cancelButton = document.getElementById('cancelbtn');
    cancelButton.addEventListener('click', function() {
      const pathName = window.location.pathname;
      const URL = window.location.protocol + '//' + window.location.hostname + "/" + pathName.substr(0, pathName.indexOf("add-product")).split("/")[1];
      document.location.href = URL;
    });
    const product_type = document.getElementById("productType");
    product_type.addEventListener("change", function() {
      fields.forEach(selectedField)
    })

    function selectedField(field) {
      if (field.id == product_type.value) {
        field.style.display = 'block';
      } else {
        field.style.display = 'none';
      }
    }
  </script>
</body>

</html>