<!DOCTYPE html>
<html lang="en">

<head>
  <link rel="stylesheet" type="text/css" href="<?= ASSETS ?>/css.css">
  <title>Product List</title>
  <meta charset="UTF-8">
</head>

<body>
  <header class="header">
    <div class="container header-container">
      <h1>
        Product List
      </h1>
      <ul class="nav main-nav">
        <li><button type="button" name="addBtn" id="addBtn" class="btn">ADD</button></li>
        <li><button type="submit" form="deleteForm" name="delete-product-btn" id="delete-product-btn" class="btn">MASS DELETE</button></li>
      </ul>
    </div>
  </header>

  <section class="container">
    <form method="post" id="deleteForm">
      <div class="product-items">
        <?php if (is_array($data)) : ?>
          <?php foreach ($data as $row) : ?>
            <div class="product-item">
              <input class="delete-checkbox" value=<?= $row->get_idProduct() ?> type="checkbox" name="check_selected[]">
              <p><?= $row->get_sku() ?></p>
              <p><?= $row->get_name() ?></p>
              <p><?= $row->get_price() ?></p>
              <p><?= $row->showProductData() ?></p>
            </div>
          <?php endforeach; ?>
        <?php endif ?>
      </div>
    </form>
  </section>

  <?php $this->view("footer", $data); ?>

  <script>
    const addButton = document.getElementById('addBtn');
    const checkSelected = document.getElementsByName('check_selected[]');
    addButton.addEventListener('click', function() {
      const pathName = window.location.pathname;
      console.log(pathName);
      const URL = window.location.protocol + '//' + window.location.hostname + "/" + pathName.split("/")[1] + "/add-product";
      document.location.href = URL;
    });
    const form = document.getElementById("deleteForm");
    form.addEventListener('submit', function(e) {
      let deleteData = false;
      for (const checkDelete of checkSelected) {
        if (checkDelete.checked == true) {
          deleteData = true;
          break;
        }
      }
      if (deleteData == false) {
        e.preventDefault();
      }
    });
  </script>